const express = require("express");
const adminController = require("../controller/adminController");
const db = require("../config/connection");
const collections = require("../config/collections");





const router = express.Router();

router.post("/adddata", adminController.addData);
router.get("/getdata", adminController.getData);

  

module.exports = router;

