const express = require("express");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");
const userRouter = require("./routes/user");
const adminRouter = require("./routes/admin");
const path = require("path");


const app = express();

dotenv.config();

const db = require("./config/connection");


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

db.connect((err) => {
  if (err) {
    console.log("connection err", err);
  } else {
    console.log("database connected");
  }
});

app.use("/user", userRouter);
app.use("/", adminRouter);

app.use((req,res,next)=>{
  const err = new Error('Not Found');
  err.status = 404;
  next(err)
})

// error handler
app.use(function (err, req, res, next) {

  // render the error page
  res.status(err.status || 500).json({
    message:err.message || 'Somthing Error',
  });
});
const port = process.env.BACK_END_PORT

app.listen(port, console.log(`server started at: ${port}`));

module.exports = app;
